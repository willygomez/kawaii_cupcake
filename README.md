# Nombre del Sistema #

Sistema de Gestion Cafeteria Kawaii CupCake

### Integrantes ###

* William Alberto Gómez Rojas
* Jose David Sanchez Cordero
* Yendry Cascante Gómez
* Alejandro Esquivel Montero

### Descripción del proyecto ###

* Módulo de Inventario
* Módulo de Gestión de Clientes
* Módulo de Facturas
* Módulo de Análisis de Datos
* Módulo de Punto de Venta
* Módulo de Gestión de Envíos
* Módulo de Autenticacion

### Cómo instalar el repositorio en el equipo para desarrollo ###

* $ git clone link
* $ cd nombre_carpeta_proyecto
* $ npm install
* $ npm i -D electron@latest
* $ npm i promise-mysql
* $ npm install sweetalert2
