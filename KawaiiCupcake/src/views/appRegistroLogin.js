const {ipcRenderer,ipcMain}=require ("electron")
const Swal = require('sweetalert2')

const RegisterForm = document.getElementById('RegisterForm');
const UserUsuario = document.getElementById('usuario');
const UserClave = document.getElementById('clave');
const UserRepetirClave = document.getElementById('repetir_clave');


async function Registrar(){

    const newUser = {
        usuario: UserUsuario.value,
        clave: UserClave.value,
        repetir_clave: UserRepetirClave.value
    }

try {
    const result = await ipcRenderer.send('register', newUser);

    Swal.fire({
        title: 'REGISTRADO',
        text: 'Usuario registrado',
        icon: 'success',
        
      })

} catch (error) {
    
    Swal.fire({
        title: 'Error!',
        text: error,
        icon: 'error',
      })
}
}