const {ipcRenderer,ipcMain}=require ("electron")

const Swal = require('sweetalert2')

const EnviosForm=document.getElementById("EnviosForm")
const TipoEn=document.getElementById("tipo")
const EstadoEn=document.getElementById("estado")
const FechaEn=document.getElementById("fecha")
const FacturaEn=document.getElementById("id_factura")

const EnviosList = document.getElementById("TableEnvios")
let envios =[]
let EnvioEditable=""
let EditingStatus=false;

var id = 0;

//---------------------FUNCIONES DEL MODULO---------------------//

EnviosForm.addEventListener('submit',async(e)=>{

    const NuevoEnvio={

        tipo:TipoEn.value,
        estado:EstadoEn.value,
        fecha:FechaEn.value,
        id_factura:FacturaEn.value

    }
    console.log(NuevoEnvio)
  if(!EditingStatus){

    const Result=await ipcRenderer.send('AñadirEnvios',NuevoEnvio)

  }else{

    const resultado = await ipcRenderer.send('EditarEnvio',EnvioEditable,NuevoEnvio)
    EditingStatus=false;
    EnvioEditable='';

  }

    EnviosForm.reset();  
    location.reload();
})

ipcRenderer.on('ListaEnvios', (event, ListadeEnvios) => {
    envios=ListadeEnvios;

    RenderEnvios(envios)

})


function RenderEnvios(envios){

    //tabla de inventario
    const table = $('#VerEnvios').DataTable();
    
    //limpiar tabla de datos viejos
    table.clear().draw();
    
    $(document).ready(function() {
    
        envios.forEach(envios => {
        
          var dia = normalizarFecha(envios.fecha, 'dd/MM/yyyy')

          //Ingresar datos a la tabla
          table.row.add( [
                envios.Id_envio,
                envios.tipo,
                envios.estado,
                dia,
                envios.Id_factura,
                '<p><button class ="btn btn-danger" onclick="DangerActive('+envios.Id_envio+')">Eliminar'+
                '</button><button class ="btn btn-info" onclick="ConsultaEnvios('+
                '\''+envios.Id_envio+'\','+
                '\''+envios.tipo+'\','+
                '\''+envios.estado+'\','+
                '\''+envios.fecha+'\','+
                '\''+envios.Id_factura+'\','+
                ');">Editar</button></p>'
                ] ).draw( false );
        } );
    } );
    }

async function deleteEnvio(){


        const Delete = await ipcRenderer.send('DeleteEnvio',id);
        location.reload();
 
   
}

ipcRenderer.on('DatosConsultaIDEnvio', (event, envios) => {
    ConsultaId=envios;
})

async function ConsultaEnvios(id,tipo,estado,fecha,id_factura){

    EditingStatus = true
    EnvioEditable=id;

    TipoEn.value=tipo;
    EstadoEn.value=estado;
    FechaEn.value=fecha;
    FacturaEn.value=id_factura;
}

function normalizarFecha(fecha, formato) {
    const date = new Date(fecha);

    //extraer cada parte de la fecha
    const day = date.getDate();
    const month = date.getMonth() + 1;
    const year = date.getFullYear();    

    //remplazar mes
    formato = formato.replace("MM", month.toString().padStart(2,"0"));        

    //remplazar año
    if (formato.indexOf("yyyy") > -1) {
        formato = formato.replace("yyyy", year.toString());
    } else if (formato.indexOf("yy") > -1) {
        formato = formato.replace("yy", year.toString().substr(2,2));
    }

    //remplazar dia
    formato = formato.replace("dd", day.toString().padStart(2,"0"));

    return formato;
}

ipcRenderer.on('ListaVentas', (event, ListaVentas) => {
    Ventas=ListaVentas;


    RenderVentas(Ventas)

})

function RenderVentas(Ventas){

    Ventas.forEach(Ventas => {
        FacturaEn.innerHTML += `

        <option value="${Ventas.idFactura}">${Ventas.idFactura} - ${Ventas.idCliente}</option>
      `;

    });
}

function DangerActive(envio){
    id = envio
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })
      
      swalWithBootstrapButtons.fire({
        title: 'Eliminar Envío',
        text: 'Estas seguro de eliminar el envío número"'+ id +'"',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
        reverseButtons: true
      }).then((result) => {
        if (result.isConfirmed) {
          swalWithBootstrapButtons.fire({
            title:'Eliminado!',
            text:'Se elimino el envío número "'+ id +'"',
            icon:'success',
            confirmButtonText: 'Ok'
          }).then((result) => {
            if (result.isConfirmed) {
                deleteEnvio();
                }
          })
          
        } else if (
          result.dismiss === Swal.DismissReason.cancel
        ) {
          
        }
      })

    //document.getElementById('msgEliminar').innerHTML = 'Deseas eliminar el envío numero <b>' + id + '</b>';
    
    //document.getElementById('overlay').classList.add('active');
    //document.getElementById('popup').classList.add('active');
}

function DangerDesactive(){
    document.getElementById('overlay').classList.remove('active');
    document.getElementById('popup').classList.remove('active');
}