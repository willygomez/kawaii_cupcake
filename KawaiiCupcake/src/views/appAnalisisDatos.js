
const {ipcRenderer,ipcMain}=require ("electron")

Pie={
    Producto: [],
    Cantidad:[],
    color:[]
}


/*colores={
    backgroundColor: ["rgba(255, 0, 0, 0.8)", "rgba(100, 255, 0, 0.5)", "rgba(200, 50, 255, 0.8)", "rgba(0, 100, 255, 0.5)", "rgba(255, 230, 0, 0.)"]
}*/

ipcRenderer.on('ListaGrafico', (event,Total ) => {
    GraficoTotal = Total;

    var i = 0;
    //var c = 0;

    GraficoTotal.forEach(GraficoTotal => {
        Pie.Producto[i] = GraficoTotal.nombre
        Pie.Cantidad[i] = GraficoTotal.total
        Pie.color[i] = "rgba("+Math.floor(Math.random() * 225)+","+ Math.floor(Math.random() * 225)+","+ Math.floor(Math.random() * 225)+", 0.4)"
        i += 1;
        /*c += 1;
        if(c >= colores.backgroundColor.length){
            c = 0
        }*/
        
    })
    Grafico();
})

ipcRenderer.on('TotalIngresos', (event,TotalIngresos ) => {
    Ingresos = TotalIngresos;

    RenderIngresos(Ingresos)
})

ipcRenderer.on('TotalVentasRealizadas', (event,TotalVentasRealizadas) => {
    Ventas = TotalVentasRealizadas;

    RenderVentas(Ventas)
})

ipcRenderer.on('TotalClientes', (event,TotalClientes ) => {
    Clientes = TotalClientes;

    RenderClientes(Clientes)
})

ipcRenderer.on('TotalEnvios', (event,TotalEnvios ) => {
    Envios = TotalEnvios;

    RenderEnvios(Envios)

})


document.addEventListener("DOMContentLoaded", function(event) { 
    
});


function RenderIngresos(Ingresos){

    Ingresos.forEach(Ingresos => {

        if(Ingresos.total == null){
            Ingresos.total = 0
        }

        var ingreso = document.getElementById('ganancias')
        ingreso.innerHTML = '';
        ingreso.innerHTML += `<p><i class="fa-solid fa-hand-holding-dollar fa-4x iconos"></i><label id="ganancias" class="datos xl">Ganancias: ₡${Ingresos.total}</label></p>`

    })
}

function RenderVentas(Ventas){
    Ventas.forEach(Ventas => {

    if(Ventas.ganancias == null){
        Ventas.ganancias = 0
    }

    var venta = document.getElementById('ventas')
    venta.innerHTML = '';
    venta.innerHTML += `<p><i class="fa-solid fa-sack-dollar fa-4x iconos"></i><label id="ventas" class="datos xl">Ventas: ${Ventas.ganancias} </label></p>`
    
    })
}

function RenderClientes(Clientes){
    Clientes.forEach(Clientes => {

        if(Clientes.Clientes == null){
            Clientes.Clientes = 0
        }

        var clientes = document.getElementById('clientes')
        clientes.innerHTML = '';
        clientes.innerHTML += `<p><i class="fa-solid fa-users fa-4x iconos"></i><label id="clientes" class="datos xl">Clientes: ${Clientes.Clientes - 1}</label></p>`
    })
}

function RenderEnvios(Envios){
    Envios.forEach(Envios => {

        if(Envios.Envios == null){
            Envios.Envios = 0
        }

        var envios = document.getElementById('envios')
        envios.innerHTML = '';
        envios.innerHTML += `<p><i class="fa-solid fa-truck-ramp-box fa-4x iconos"></i><label id="envios" class="datos xl">Envíos: ${Envios.Envios}</label></p>`

    })
}


function Grafico(){
    var ctx = $("#chart-line");
    var myLineChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: Pie.Producto,
            datasets: [{
                data: Pie.Cantidad,
                backgroundColor: Pie.color
            }]
        },
        options: {
            title: {
                display: true,
                text: 'Productos'
            }
        }
    });
};




