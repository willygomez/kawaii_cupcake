const {ipcRenderer,ipcMain}=require ("electron")

const Swal = require('sweetalert2')

//----OBJETOS DEL FORMULARIO----//

const ClientForm=document.getElementById("ClientForm")
const ClientName=document.getElementById("Nombre")
const ClientApe1=document.getElementById("Apellido1")
const ClientApe2=document.getElementById("Apellido2")
const ClientMail=document.getElementById("Correo")


const ClientList = document.getElementById("TableClientes")
let Clientes =[]
let ClienteEditable=""
let EditingStatus=false;

var id = 0;



//---------------------FUNCIONES DEL MODULO---------------------//

ClientForm.addEventListener('submit',async(e)=>{

    const NuevoCliente={
        Nombre:ClientName.value,
        Primer_Apellido:ClientApe1.value,
        Segundo_Apellido:ClientApe2.value,
        Correo:ClientMail.value
    }

  if(!EditingStatus){

    const Result=await ipcRenderer.send('AñadirClientes',NuevoCliente)

  }else{

    const resultado = await ipcRenderer.send('EditarCliente',ClienteEditable,NuevoCliente)
    EditingStatus=false;
    ClienteEditable='';

  }

    ClienteForm.reset();  
    location.reload();
})

ipcRenderer.on('ListaClientes', (event, ListadeClientes) => {
    Clientes=ListadeClientes;

    console.log(Clientes);

    RenderClientes(Clientes)


})



function RenderClientes(Clientes){

    //tabla de inventario
    const table = $('#VerClientes').DataTable();
    
    //limpiar tabla de datos viejos
    table.clear().draw();
    
    $(document).ready(function() {
    
        Clientes.forEach(Clientes => {
          //Ingresar datos a la tabla
          if(Clientes.Id_Cliente==0){

          }else{

          
          table.row.add( [
                Clientes.Id_Cliente,
                Clientes.Nombre,
                Clientes.Primer_Apellido,
                Clientes.Segundo_Apellido,
                Clientes.Correo,
                '<p><button class ="btn btn-danger" onclick="DangerActive('+Clientes.Id_Cliente+',Clientes)">Eliminar'+
                '</button><button class ="btn btn-info" onclick="ConsultaClientes('+
                '\''+Clientes.Id_Cliente+'\','+
                '\''+Clientes.Nombre+'\','+
                '\''+Clientes.Primer_Apellido+'\','+
                '\''+Clientes.Segundo_Apellido+'\','+
                '\''+Clientes.Correo+'\','+
                ');">Editar</button></p>'
            ] ).draw( false );
        }} );
    } );
    }

async function deleteCliente(){

        const Delete = await ipcRenderer.send('DeleteCliente',id);
        location.reload();
   
}

ipcRenderer.on('DatosConsultaIDCliente', (event, Cliente) => {
    ConsultaId=Cliente;
})

async function ConsultaClientes(id,Nombre,Apellido1,Apellido2,Correo){

    EditingStatus = true
    ClienteEditable=id;

    ClientName.value=Nombre;
    ClientApe1.value=Apellido1;
    ClientApe2.value=Apellido2;
    ClientMail.value=Correo;
    
}


function DangerActive(cliente, Clientes){
    id = cliente
    Clientes.forEach(Clientes => {
        if(Clientes.Id_Cliente == cliente){
            //document.getElementById('msgEliminar').innerHTML = 'Deseas eliminar al cliente <b>' + Clientes.Nombre+'</b>';

            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                  confirmButton: 'btn btn-success',
                  cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
              })
              
              swalWithBootstrapButtons.fire({
                title: 'Eliminar Cliente',
                text: 'Estas seguro de eliminar al cliente"'+ Clientes.Nombre +'"',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Si',
                cancelButtonText: 'No',
                reverseButtons: true
              }).then((result) => {
                if (result.isConfirmed) {
                  swalWithBootstrapButtons.fire({
                    title:'Eliminado!',
                    text:'Se elimino al cliente "'+ Clientes.Nombre +'"',
                    icon:'success',
                    confirmButtonText: 'Ok'
                  }).then((result) => {
                    if (result.isConfirmed) {
                        deleteCliente();
                        }
                  })
                  
                } else if (
                  result.dismiss === Swal.DismissReason.cancel
                ) {
                  
                }
              })
        }
      } );

    //document.getElementById('overlay').classList.add('active');
    //document.getElementById('popup').classList.add('active');
}//

function DangerDesactive(){
    document.getElementById('overlay').classList.remove('active');
    document.getElementById('popup').classList.remove('active');
}