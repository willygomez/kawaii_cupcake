const  {ipcRenderer,ipcMain} = require("electron")

const Swal = require('sweetalert2')

const InventoryForm = document.getElementById('InventoryForm');
const ProductCategoria=document.getElementById('Categoria');
const ProductNombre=document.getElementById('Nombre');
const ProductPrecio=document.getElementById('Precio');
const ProductStock=document.getElementById('Stock');

//--------------------------------------------//
const productslist= document.getElementById('productos')
let Products = []
let ProductoEditable='';

let EditingStatus=false;

var id = 0;

Titulo = "Producto Ingresado"
mensage = "producto ingresado con exito"

//-------------------------------------------//




//-------------------------------------------//

InventoryForm.addEventListener('submit',async(e)=>{
    
    
const NuevoProducto={
    Categoria: ProductCategoria.value,
    Nombre:ProductNombre.value,
    Precio:ProductPrecio.value,
    Stock:ProductStock.value
}

//----

    if (!EditingStatus){
        const result = await ipcRenderer.send('msg',NuevoProducto);
    }
    else{

       const resultado = await ipcRenderer.send('Editar',ProductoEditable,NuevoProducto)

       EditingStatus=false;
       ProductoEditable='';
    }

//----


InventoryForm.reset();


//--Solucion Provicional al reset de los productos--//
location.reload();



})

//---------------------------------------------------------------------//


    ipcRenderer.on('Productos', (event, ListadeProductos) => {
        Products=ListadeProductos;

        RenderProducts(Products)

      
    })




//---------------------------------------------------------------------//

async function deleteProduct(){
        const Delete = await ipcRenderer.send('Delete',id);
        location.reload();

   
}

//------------------------------------------------------------------------------

async function ConsultaProducts(id,Cat,Nom,Pre,Stock){
   
    console.log(Cat);
    console.log(Nom);
    EditingStatus = true

    ProductoEditable=id;

    ProductCategoria.value = Cat;
    ProductNombre.value = Nom;
    ProductPrecio.value = Pre;
    ProductStock.value = Stock;
}






async function EditProducts(ID){



  
    

}

ipcRenderer.on('DatosConsultaID', (event, Producto) => {
    

    ConsultaId=Producto;

    EditProducts(ConsultaId);

    
   

  
})



function RenderProducts(Products){

    var f = "hola";
    //tabla de inventario
    const table = $('#VerProductos').DataTable();
    
    //limpiar tabla de datos viejos
    table.clear().draw();
    
    $(document).ready(function() {
    
        Products.forEach(Products => {
          //Ingresar datos a la tabla
          table.row.add( [
                Products.Nombre.toString(),
                Products.Categoria.toString(),
                '₡'+ Products.Precio,
                Products.Stock,
                '<p><button class ="btn btn-danger" onclick="DangerActive('+Products.Id_Producto+',Products)">Eliminar'+
                '</button><button class ="btn btn-info" onclick="ConsultaProducts('+
                '\''+Products.Id_Producto+'\','+
                '\''+Products.Categoria+'\','+
                '\''+Products.Nombre+'\','+
                '\''+Products.Precio+'\','+
                '\''+Products.Stock+'\','+
                ');">Editar</button></p>'
            ] ).draw( false );
        } );
    } );
    }

    function SuccessActive(){
        document.getElementById('tituloS').innerHTML = Titulo;
        document.getElementById('mensageS').innerHTML = mensage;

        document.getElementById('succes').classList.add('active');
	    document.getElementById('succes-pop').classList.add('active');
    }
    
    function SuccessDesactive(){
        document.getElementById('succes').classList.remove('active');
	    document.getElementById('succes-pop').classList.remove('active');
    }

    function DangerActive(producto, Products){

        id = producto
        Products.forEach(Products => {
            if(Products.Id_Producto == producto){
                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                      confirmButton: 'btn btn-success',
                      cancelButton: 'btn btn-danger'
                    },
                    buttonsStyling: false
                  })
                  
                  swalWithBootstrapButtons.fire({
                    title: 'Eliminar Producto',
                    text: 'Estas seguro de eliminar el producto "'+ Products.Nombre+'"',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Si',
                    cancelButtonText: 'No',
                    reverseButtons: true
                  }).then((result) => {
                    if (result.isConfirmed) {
                      swalWithBootstrapButtons.fire({
                        title:'Eliminado!',
                        text:'Se elimino el producto "'+ Products.Nombre+'"',
                        icon:'success',
                        confirmButtonText: 'Ok'
                      }).then((result) => {
                        if (result.isConfirmed) {
                            deleteProduct();
                            }
                      })
                      
                    } else if (
                      result.dismiss === Swal.DismissReason.cancel
                    ) {
                      
                    }
                  })
            }
          } );

        //document.getElementById('overlay').classList.add('active');
	    //document.getElementById('popup').classList.add('active');
    }
    
    function DangerDesactive(){
        document.getElementById('overlay').classList.remove('active');
	    document.getElementById('popup').classList.remove('active');
    }

    function WarningActive(){
        document.getElementById('overlay').classList.add('active');
	    document.getElementById('popup').classList.add('active');
    }
    
    function WarningDesactive(){
        document.getElementById('overlay').classList.remove('active');
	    document.getElementById('popup').classList.remove('active');
    }
    
    function ValidaSoloNumeros() {
        if ((event.keyCode < 48) || (event.keyCode > 57)) 
         event.returnValue = false;
    }

    