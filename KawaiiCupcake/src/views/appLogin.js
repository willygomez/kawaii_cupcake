const { ipcRenderer } = require('electron');

const Swal = require('sweetalert2')


const LoginForm = document.getElementById('LoginForm');
const User = document.getElementById('usuario');
const Pass = document.getElementById('clave');


let Logear = false;
LoginForm.addEventListener('submit', async (e) => {
    e.preventDefault();
    const USUARIO =User.value;
    const PASSWORD =Pass.value;
    const envio = await ipcRenderer.send('Login',USUARIO,PASSWORD);

});


ipcRenderer.on('LOGEAR', (event, ESTADO) => {
    

    LOGEAR(ESTADO);
    
})


function LOGEAR(ESTADO){

if(ESTADO==true){

  

    window.location.href = "index.html";

}else{
   
    Swal.fire({
        title: 'Error!',
        text: 'Usuario o Contraseña Incorrecta',
        icon: 'error',
        confirmButtonText: 'Reintentar'
      })

}
    
}