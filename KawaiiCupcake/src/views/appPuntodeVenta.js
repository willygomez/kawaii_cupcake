const {ipcRenderer,ipcMain}=require ("electron")

require('events').EventEmitter.defaultMaxListeners = 15;

const Swal = require('sweetalert2')

let Products =[];
let Clientes =[];
let Facturas =[];

Productos={
    Id: [],
    Categoria: [],
    Nombre:[],
    Precio: [],
    Stock: []
}

const ComboProductos = document.getElementById("IdProductos")
const ComboClientes = document.getElementById("IdClientes")
const cantidad_txt = document.getElementById("Cantidad");
const precio_txt = document.getElementById("Precios");
const combo = document.getElementById("IdProductos");

const PreciosProductos = document.getElementById("Precios")

const PuntoVentaForm=document.getElementById("PuntoVentaForm")
const VENTATOTAL=document.getElementById("Total")
const VENTATOTAL2=document.getElementById("TotalFinal")
const VENTACLIENTE=document.getElementById("IdClientes")
const IdFactura=document.getElementById("IdFactura")


const btn = document.getElementById("addRow");

let EditingStatus=false;

IdFacturas={
    idFactura: []
}

var Total = 0;
var cantidades = 0;
const mensage = 0;


//Obtener Facturas

ipcRenderer.on('ListaVentas', (event, ListaVentas) => {
    Facturas=ListaVentas;
    var i = 0;

    Facturas.forEach(Facturas => {
        IdFacturas.idFactura[i] = Facturas.idFactura
        i += 1;
    })

    mensageF();

})

function mensageF(){
    if(mensage == 1){
        Swal.fire({
            title: 'FACTURA',
            text: 'Factura Creada',
            icon: 'success',
            
          })
    }else if(mensage == 2){
        saltar = true
        Swal.fire({
            title: 'Error!',
            text: error,
            icon: 'error',
            })
    }else{
        
    }
}

//------------------GET PRODUCTS------------------//

PuntoVentaForm.addEventListener('submit',async(e)=>{

    //Variables para contar cuantos productos hay

    var dia = normalizarFecha(new Date());

    var table = $('#VerPuntoVenta').DataTable();
    var data = table.rows().data();
    var stop = data.length;
    var saltar = false;

    //Ingresar factura

    const NuevaVenta={
        idFactura: IdFactura.value,
        Total: parseInt(document.getElementById("Total").value.substr(1)),
        idCliente: VENTACLIENTE.options[VENTACLIENTE.selectedIndex].text,
        fecha: dia
    }

    if(Validar(IdFactura.value)){

    const Result=await ipcRenderer.send('RealizarVenta',NuevaVenta)
    

    //Ingresar Detalle de Factura
        for (var i = 0; i < stop; i++) {

            //Datos del Detalle
            var NuevoDetalle={
                idFactura: IdFactura.value,
                idProducto: parseInt(data[i][0]),
                nombre: data[i][1],
                precio: parseInt(data[i][2].substr(1)),
                Cantidad: parseInt(data[i][3]),
                SubTotal: parseInt(data[i][4].substr(1))
            }

            for(var p = 0; p < Productos.Id.length; p++){
                if(Productos.Id[p] == parseInt(data[i][0])){
                    var totalResta = Productos.Stock[p] - parseInt(data[i][3])
                }
            }
            
            //Datos Editados de productos
            var NuevoProducto={
                Stock: totalResta
            }
            
            
            
            
        const Result2=await ipcRenderer.send('AñadirDetalle',NuevoDetalle)
    
        const resultado = await ipcRenderer.send('Editar',parseInt(data[i][0]),NuevoProducto)
    
        }
    
        table.clear();
       PuntoVentaForm.reset();  
      location.reload();

    mensage = 1

    }else{
        mensage = 2 
    }
    
})




function Validar(idActual){
    for(var i = 0; i<IdFacturas.idFactura.length; i++){
        if(idActual == IdFacturas.idFactura[i]){
            return false
        }
    }
    return true
}




//Funcion que trae todos los productos

ipcRenderer.on('ProductosPuntoVenta', (event, ListadeProductos) => {
    Products=ListadeProductos;
    var i = 0;

    Products.forEach(Products => {
        Productos.Id[i] = Products.Id_Producto
        Productos.Categoria[i] = Products.Categoria
        Productos.Nombre[i] = Products.Nombre
        Productos.Precio[i] = Products.Precio
        Productos.Stock[i] = Products.Stock
        i += 1;
    })

    RenderProducts(Products)
    actualizar();
})


// llenar combo de productos
function RenderProducts(Products){
    ComboProductos.innerHTML='<option value="Seleccionar Producto">Seleccionar Producto</option>';
    Products.forEach(Products => {

        ComboProductos.innerHTML += `

        <option value="${Products.Id_Producto}">${Products.Nombre}</option>
      `;

    });
    
   

}

//Funcion que ingresa productos a la tabla del carrito

function IngresarAlCArro(){

    //tabla de Carrito
    const table = $('#VerPuntoVenta').DataTable();
    var cantidad = cantidad_txt.value;
    var precio = precio_txt.value;
    var SubTotal = parseInt(precio.substr(1)) * parseInt(cantidad);
    var NombreProducto = combo.options[combo.selectedIndex].text;
    var id = combo.value
    var data = table.rows().data(); 
    var datos = ""; 
    var verificar = true;
    if(cantidad == ""){

        Swal.fire({
            title: 'Error!',
            text: 'No ha ingresado la cantidad del producto',
            icon: 'warning',
            confirmButtonText: 'Reinterntar'
          })

    }else{

    
    if(cantidad < cantidades+1){

    // si la tabla no tiene ningun producto agregar el primer producto
    $(document).ready(function() {
    
    if(data.length <= 0){
        table.row.add( [
            id,
            NombreProducto,
            precio,
            cantidad,
            '₡'+ SubTotal,
            '<p><button type="button" class ="btn btn-danger" onclick="eliminarC('+id+')">Eliminar</p>'
        ] ).draw( true );

    //si no valida que el producno se haya ingreado
    }else{
        for (var i = 0; i < data.length; i++) {
            datos = data[i]; 
            
            // si ya se ingreso actualiza los valores para que no se duplique el producto en la tabla

            if(id == datos[0]){

                //Validad catidad de inventarios
                if(parseInt(data[i][3],10)+parseInt(cantidad) <= cantidades){

                    //actualiza la cantidad
                    var cantidadA = parseInt(data[i][3],10) + parseInt(cantidad, 10);
                    data[i][3] = cantidadA;

                    //actualiza el precio
                    var precioA = parseInt(data[i][2].substr(1)) * parseInt(data[i][3]);
                    data[i][4] = '₡' + precioA;
                    verificar = false;

                    //actualiza los datos de la tabla
                    actualizar();
                }else{
                    verificar = false;
                    Swal.fire({
                        title: 'Error!',
                        text: 'Solo existen '+cantidades+' productos de '+NombreProducto+' en inventario ',
                        icon: 'warning',
                        confirmButtonText: 'Reinterntar'
                      })
                }
            }
            
        }
        
        // si el producto no esta en la tabla previamente lo inserta

        if(verificar){
            //Ingresar datos a la tabla
            table.row.add( [
                id,
                NombreProducto,
                precio,
                cantidad,
                '₡' + SubTotal,
                '<p><button type="button" class ="btn btn-danger" onclick="eliminarC('+id+')">Eliminar</p>'
            ] ).draw( true );
        }
    }
    } );
    Total = Total + SubTotal;
    document.getElementById("Total").value = '₡' + Total;
}else{
    Swal.fire({
        title: 'Error!',
        text: 'Solo existen '+cantidades+' productos de '+NombreProducto+' en inventario',
        icon: 'warning',
        confirmButtonText: 'Reinterntar'
      })
}
}
}

function Error(){
    
}



function prueba(){
    var combo = document.getElementById("IdProductos");
    var text = document.getElementById("Precios");
    var cantidad = cantidad_txt.value
    var selected = combo.options[combo.selectedIndex].value;

    Products.forEach(Products => {

        if(selected == "Seleccionar Producto"){
            text.value = '₡0'
            btn.disabled=true
        }else if(selected == Products.Id_Producto){
            text.value = '₡' + Products.Precio
            cantidades = Products.Stock
            btn.disabled=false
        }

    });


  }


function actualizar(){
    const table = $('#VerPuntoVenta').DataTable();
    var data = table.rows().data();
    var stop = data.length;
    var tableName = {
        id:[],
        nombre: [],
        precio: [],
        cantidad: [],
        subtotal: []
      }

    for (var i = 0; i < stop; i++) {
        tableName.id[i] = data[i][0]
        tableName.nombre[i] = data[i][1]
        tableName.precio[i] = data[i][2]
        tableName.cantidad[i] = data[i][3]
        tableName.subtotal[i] = data[i][4]
    }

    

    table.clear();


    for (var i = 0; i < stop; i++) {
        //Ingresar datos a la tabla
        var id = tableName.id[i]
        var nombre = tableName.nombre[i]
        var precio = tableName.precio[i]
        var cantidad = tableName.cantidad[i]
        var subtotal = tableName.subtotal[i]

        table.row.add( [
        id,
        nombre,
        precio,
        cantidad,
        subtotal,
        '<p><button type="button" class ="btn btn-danger" onclick="eliminarC('+id+')">Eliminar</p>'
        ] ).draw( true );
        
    }
}


//-------------------------------Clientes Punto Venta--------------//

ipcRenderer.on('ClientesPuntoVenta', (event, ListadeClientes) => {
    Clientes=ListadeClientes;


    RenderClientes(Clientes)

})

function RenderClientes(Clientes){

    ComboClientes.innerHTML = ''
    
    Clientes.forEach(Clientes => {

        if(Clientes.Id_Cliente == 0){
            ComboClientes.innerHTML += `

        <option value="${Clientes.Id_Cliente}">${Clientes.Nombre}</option>
      `;

        }else{

        ComboClientes.innerHTML += `

        <option value="${Clientes.Id_Cliente}">${Clientes.Nombre} ${Clientes.Primer_Apellido}</option>
      `;

    }});
}

function prueba2(){
    Total = 0;
    const table = $('#VerPuntoVenta').DataTable();
    table.clear().draw( true );
}


function normalizarFecha(current_datetime) {

    let formatted_date = current_datetime.getFullYear() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getDate() + " " + current_datetime.getHours() + ":" + current_datetime.getMinutes() + ":" + current_datetime.getSeconds();
    
    return formatted_date;


    return formato;
}

ipcRenderer.on('ListaVentas', (event, ListaVentas) => {
    Facturas=ListaVentas;


    MostrarFactura(Facturas);
})

function MostrarFactura(Facturas){
    Facturas.forEach(Facturas => {

        var factura = document.getElementById('IdFactura')
        factura.value = Facturas.idFactura + 1;

    })
}

function ValidaSoloNumeros() {
    if ((event.keyCode < 48) || (event.keyCode > 57)) 
     event.returnValue = false;
}


        

function eliminarC(id){

    var table = $('#VerPuntoVenta').DataTable();
    var data = table.rows().data(); 
    var datos = ""; 


    for (var i = 0; i < data.length; i++) {
        datos = data[i]; 
        // si ya se ingreso actualiza los valores para que no se duplique el producto en la tabla

        if(id == datos[0]){

            //Validad catidad de inventarios
            
            table.row(':eq('+i+')').remove().draw();
            actualizar();
            Total = Total - parseInt(data[i][2].substr(1))
            document.getElementById("Total").value = '₡' + Total;
            document.getElementById("TotalFinal").value = '₡' + Total;

    
    }
}

}
