const {ipcRenderer,ipcMain}=require ("electron")

//----OBJETOS DEL FORMULARIO----//

var btnAbrirPopup = document.getElementById('btn-abrir-popup'),
	overlay = document.getElementById('overlay'),
	popup = document.getElementById('popup'),
	btnCerrarPopup = document.getElementById('btn-cerrar-popup');


Detalles={
    idFactura: [],
    idProducto:[],
    producto: [],
    precio: [],
    Cantidad: [],
    SubTotal: []
}


ipcRenderer.on('ListaDetalle', (event, ListaDetalle) => {
    Detalle=ListaDetalle;
    var i = 0;

    Detalle.forEach(Detalle => {
        Detalles.idFactura[i] = Detalle.idFactura
        Detalles.idProducto[i] = Detalle.idProducto
        Detalles.producto[i] = Detalle.nombre
        Detalles.precio[i] = Detalle.Precio
        Detalles.Cantidad[i] = Detalle.Cantidad
        Detalles.SubTotal[i] = Detalle.SubTotal
        i += 1;
    })
    
})

ipcRenderer.on('ListaVentas', (event, ListaVentas) => {
    Facturas=ListaVentas;

    

    RenderVentas(Facturas);
})

function RenderVentas(Facturas){
    
    //tabla de inventario
    const table = $('#VerFacturas').DataTable();
    
    //limpiar tabla de datos viejos
    table.clear().draw();
    
    $(document).ready(function() {
    
        
        Facturas.forEach(Facturas => {
          //Normalizar Fecha
          
          //var dia = normalizarFecha(Facturas.fecha, 'yyyy-MM-dd')
          var dia = Facturas.fecha.toLocaleString();

          //Ingresar datos a la tabla
          if(Facturas.Nombre == 'Sin Cliente'){
            table.row.add( [
                Facturas.idFactura,
                Facturas.idCliente,
                '₡' + Facturas.Total,
                dia,
                '<button onclick="activar(); RenderDetalle(Detalles,'+Facturas.idFactura+');" id="tn-abrir-popup" class="btn btn-info btn-abrir-popup">Ver Detalles</button>'
                   
                ] ).draw( false );
          }else{
              
          table.row.add( [
            Facturas.idFactura,
            Facturas.idCliente,
            '₡' + Facturas.Total,
            dia,
            '<button onclick="activar(); RenderDetalle(Detalles,'+Facturas.idFactura+',' +Facturas.Total+');" id="tn-abrir-popup" class="btn btn-info btn-abrir-popup">Ver Detalles</button>'
               
            ] ).draw( false );
        }} );
    } );
}

function RenderDetalle(Detalles,idFactura,totalDetalle){
    //tabla de inventario
    const table = $('#VerDetalleVenta').DataTable();
    document.getElementById("datosDetalle").innerHTML = "<i><u># Factura</u></i> : " + idFactura +" <i> <u>Total</u></i> : " + totalDetalle;
    
    //limpiar tabla de datos viejos
    table.clear().draw();
    
    $(document).ready(function() {
    
        for (var i = 0; i < Detalles.idFactura.length; i++) {
          //Ingresar datos a la tabla
            if(Detalles.idFactura[i] == idFactura){
                
            table.row.add( [
                Detalles.idProducto[i],
                Detalles.producto[i],
                '₡' + Detalles.precio[i],
                Detalles.Cantidad[i],
                '₡' + Detalles.SubTotal[i]

            ] ).draw( false );
        }
        }
    } );
}


function activar(){
    overlay.classList.add('active');
	popup.classList.add('active');
}

function desactivar(){
    overlay.classList.remove('active');
	popup.classList.remove('active');
}

function normalizarFecha(fecha, formato) {
    const date = new Date(fecha);

    //extraer cada parte de la fecha
    const day = date.getDate();
    const month = date.getMonth() + 1;
    const year = date.getFullYear();    

    //remplazar mes
    formato = formato.replace("MM", month.toString().padStart(2,"0"));        

    //remplazar año
    if (formato.indexOf("yyyy") > -1) {
        formato = formato.replace("yyyy", year.toString());
    } else if (formato.indexOf("yy") > -1) {
        formato = formato.replace("yy", year.toString().substr(2,2));
    }

    //remplazar dia
    formato = formato.replace("dd", day.toString().padStart(2,"0"));

    return formato;
}

