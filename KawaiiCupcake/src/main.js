const { BrowserWindow, Notification, ipcMain, ipcRenderer } = require("electron");

const { getConnection } = require('./database')

let ConsultaUnica = [];

let LOGEAR = true


//-----------RENDERERS INVENTARIO-------------//

ipcMain.on('msg', (event, NuevoProducto) => {
    CreateProduct(NuevoProducto);
})

ipcMain.on('Delete', (event, id) => {
    deleteProduct(id);
})

ipcMain.on('Consulta', (event, id) => {
    getProductsById(id);
})

ipcMain.on('Editar', (event, ProductoEditable, NuevoProducto) => {
    EditProducts(ProductoEditable, NuevoProducto);
})

//----------------FUNCIONES DE PRODUCTOS--------------------------------//

async function CreateProduct(NuevoProducto) {
    try {
        const conn = await getConnection();
        const result = await conn.query('INSERT INTO productos set ?', NuevoProducto);
        new Notification({
            title: 'Kawaii CupCake',
            body: 'Nuevo Producto Añadido'
        }).show();
       
        GetProducts();
        GetProductsPuntoVenta();
        
    } catch (error) {
        console.log(error)
    }
}

async function GetProducts() {
    try {
        const conn = await getConnection();
        const ResultadoConsulta = await conn.query('SELECT * FROM productos')

        window.webContents.on('did-finish-load', () => {
            window.webContents.send('Productos', ResultadoConsulta)
        })

    } catch (error) {
        console.log("No sirvo Produtos")
    }
}

async function deleteProduct(id) {

    const conn = await getConnection();
    const Delete = await conn.query('DELETE FROM productos WHERE Id_Producto =?', id)
    GetProducts();
    GetProductsPuntoVenta();
    GetDatosAnalisis();
    GetGrafico();
}


async function getProductsById(id) {
    const conn = await getConnection();
    const ConsultaUnica = await conn.query('SELECT * FROM productos WHERE Id_Producto =?', id)
    console.log(ConsultaUnica)
    window.webContents.send('DatosConsultaID', ConsultaUnica)
}


async function EditProducts(ID, Producto) {
    const conn = await getConnection();
    const Editar = await conn.query('UPDATE productos SET ? WHERE Id_Producto =?', [Producto, ID])
    console.log(Editar);
    GetGrafico();
    GetProducts();
    GetProductsPuntoVenta();
}


//----------------------RENDERER LOGIN-------------------------------//

ipcMain.on('Login', (event, USUARIO, PASSWORD) => {
    consultarUsuario(USUARIO, PASSWORD);
    
})

ipcMain.on('register', (event, newUser) => {

    createUser(newUser);
});

//-------------------------------FUNCIONES DE LOGIN--------------------------------//
async function consultarUsuario(USUARIO, PASSWORD) {
    const conn = await getConnection();
    const result = await conn.query('SELECT * FROM usuarios WHERE usuario = ? And clave=?', [USUARIO, PASSWORD])

    if (result !=0) {
       
       
       // window.loadURL(`file://${__dirname}/views/index.html`);

       LOGEAR=true;

        window.webContents.send('LOGEAR', LOGEAR)

       
        
       
    } else {
        LOGEAR=false;
        window.webContents.send('LOGEAR', LOGEAR)
       
    }
    
}

// Crear registro de usuario

async function createUser(newUser) {
    try {
        const conn = await getConnection();
        const result = await conn.query('INSERT INTO usuarios set ?', newUser);
        new Notification({
            title: 'Kawaii CupCake',
            body: 'Nuevo Usuario Añadido'
        }).show();
    } catch (error) {
        console.log(error)
    }
}

//----------------RENDERERS CLIENTES------------------//

ipcMain.on('AñadirClientes', (event, NuevoCliente) => {
    AñadirClientes(NuevoCliente)
})

ipcMain.on('DeleteCliente', (event, id) => {
    deleteCliente(id);
})

ipcMain.on('EditarCliente', (event, ClienteEditable, NuevoCliente) => {
    EditClientes(ClienteEditable, NuevoCliente);
})

//--------------------------------CLIENTES-------------------------//

async function AñadirClientes(NuevoCliente) {
    try {
        const conn = await getConnection();
        const result = await conn.query('INSERT INTO clientes set ?', NuevoCliente);
        new Notification({
            title: 'Kawaii CupCake',
            body: 'Nuevo Cliente Añadido'
        }).show();
       
        GetClientes();
        GetClientesPuntoVenta();
        
    } catch (error) {
        console.log(error)
    }
    GetDatosAnalisis();
}

async function GetClientes() {
    try {
        const conn = await getConnection();
        const ResultadoConsulta = await conn.query('SELECT * FROM clientes')

        window.webContents.on('did-finish-load', () => {

            window.webContents.send('ListaClientes', ResultadoConsulta)
        })

    } catch (error) {
        console.log("No sirvo Clientes")
    }
}

async function deleteCliente(id) {

    const conn = await getConnection();
    const Delete = await conn.query('DELETE FROM clientes WHERE Id_Cliente =?', id)

    GetClientes();
    GetClientesPuntoVenta();
    GetDatosAnalisis();
    GetGrafico();
}

async function getClientessById(id) {
    const conn = await getConnection();
    const ConsultaUnica = await conn.query('SELECT * FROM clientes WHERE Id_Cliente =?', id)
    
    window.webContents.send('DatosConsultaIDCliente', ConsultaUnica)
}

async function EditClientes(ID, Cliente) {
    const conn = await getConnection();
    const Editar = await conn.query('UPDATE clientes SET ? WHERE Id_Cliente =?', [Cliente, ID])
    
    GetClientes();
    GetClientesPuntoVenta();
}

//----------------RENDERERS ENVIOS------------------//

ipcMain.on('AñadirEnvios', (event, NuevoEnvio) => {
    AñadirEnvios(NuevoEnvio)
})

ipcMain.on('DeleteEnvio', (event, id) => {
    deleteEnvio(id);
})

ipcMain.on('EditarEnvio', (event, EnvioEditable, NuevoEnvio) => {
    EditEnvios(EnvioEditable, NuevoEnvio);
})
//------------------------------ENVIOS------------------------------//

async function AñadirEnvios(NuevoEnvio) {
    try {
        const conn = await getConnection();
        const result = await conn.query('INSERT INTO envios set ?', NuevoEnvio);
        new Notification({
            title: 'Kawaii CupCake',
            body: 'Envio Añadido'
        }).show();
       
        GetEnvios();
        
    } catch (error) {
        console.log(error)
    }

    GetDatosAnalisis();
}

async function GetEnvios() {
    try {
        const conn = await getConnection();
        const ResultadoConsulta = await conn.query('SELECT * FROM envios')

        window.webContents.on('did-finish-load', () => {
            window.webContents.send('ListaEnvios', ResultadoConsulta)
        })

    } catch (error) {
        console.log("No sirvo Envios")
    }
}

async function deleteEnvio(id) {

    const conn = await getConnection();
    const Delete = await conn.query('DELETE FROM envios WHERE Id_envio =?', id)

    GetEnvios();
    GetDatosAnalisis();
    GetGrafico();
}

async function getEnviossById(id) {
    const conn = await getConnection();
    const ConsultaUnica = await conn.query('SELECT * FROM envios WHERE Id_envio =?', id)
    
    window.webContents.send('DatosConsultaIDEnvio', ConsultaUnica)
}

async function EditEnvios(ID, envios) {
    const conn = await getConnection();
    const Editar = await conn.query('UPDATE envios SET ? WHERE Id_envio =?', [envios, ID])
    
    GetEnvios();
}


//------------------------------PUNTO DE VENTA--------------------------//

//------------------------------COMBO BOXES--------------------------//

async function GetProductsPuntoVenta() {
    try {
        const conn = await getConnection();
        const ResultadoConsulta = await conn.query('SELECT * FROM productos')

        window.webContents.on('did-finish-load', () => {
            window.webContents.send('ProductosPuntoVenta', ResultadoConsulta)
        })

     

    } catch (error) {
        console.log("No sirvo Punto Venta Productos")
    }
}

async function GetClientesPuntoVenta() {
    try {
        const conn = await getConnection();
        const ResultadoConsulta = await conn.query('SELECT * FROM clientes')

        window.webContents.on('did-finish-load', () => {
            window.webContents.send('ClientesPuntoVenta', ResultadoConsulta)
        })

     

    } catch (error) {
        console.log("No sirvo Punto Venta Clientes")
    }
}

//----------------RENDERERS PUNTO DE VENTA------------------//

ipcMain.on('AñadirDetalle', (event, NuevoDetalle) => {
    AñadirDetalle(NuevoDetalle)
})

ipcMain.on('DeleteDetalle', (event, id) => {
    deleteDetalle(id);
})


//--------------------------------PUNTO DE VENTA-------------------------//


async function AñadirDetalle(NuevoDetalle) {
    try {
        const conn = await getConnection();
        const result = await conn.query('INSERT INTO detallefactura set ?', NuevoDetalle);
        
        GetDetalle();
        GetProducts();
        
    } catch (error) {
        console.log(error)
    }
    
    GetDatosAnalisis();
    GetGrafico();
}

async function GetDetalle() {
    try {
        const conn = await getConnection();
        const ResultadoConsulta = await conn.query(
            'SELECT * FROM detallefactura')

        window.webContents.on('did-finish-load', () => {

            window.webContents.send('ListaDetalle', ResultadoConsulta)
        })

    } catch (error) {
        console.log("No sirvo detallefactura")
    }
}

async function deleteDetalle(id) {

    const conn = await getConnection();
    const Delete = await conn.query('DELETE FROM detallefactura WHERE idFactura =?', id)

    GetDetalle();
}

async function getDetalleById(id) {
    const conn = await getConnection();
    const ConsultaUnica = await conn.query('SELECT * FROM detallefactura WHERE idFactura =?', id)
    
    window.webContents.send('DatosConsultaIDDetalle', ConsultaUnica)
}


//----------------------Facturas----------------------//

ipcMain.on('RealizarVenta', (event, NuevaVenta) => {

    AñadirVentas(NuevaVenta)
})

async function AñadirVentas(NuevaVenta) {
    try {
        const conn = await getConnection();
        const result = await conn.query('INSERT INTO ventas set ?', NuevaVenta);
        new Notification({
            title: 'Kawaii CupCake',
            body: 'Venta Realizada'
        }).show();
       
        GetFacturas();
        GetProducts();
        
    } catch (error) {
        console.log(error)
    }
}

async function GetFacturas() {
    try {
        const conn = await getConnection();
        const ResultadoConsulta = await conn.query('SELECT * FROM ventas')

        window.webContents.on('did-finish-load', () => {
            window.webContents.send('ListaVentas', ResultadoConsulta)
        })

    } catch (error) {
        console.log("No sirvo Ventas")
    }
}

async function GetGrafico() {
    try {
        const conn = await getConnection();
        const ResultadoConsulta = await conn.query('select a.nombre, SUM(cantidad) as total from detallefactura a, productos b where Id_Producto = idProducto GROUP BY idProducto ORDER BY total DESC')
        //select a.nombre, sum(Cantidad) as total from detallefactura a, productos b WHERE idProducto = Id_Producto GROUP BY idProducto ORDER BY idProducto;

        window.webContents.on('did-finish-load', () => {
            window.webContents.send('ListaGrafico', ResultadoConsulta)
        })

    } catch (error) {
        console.log("No sirvo Grafico")
    }
}


//------------------Analisis de Datos Consultas---------------------//

async function GetDatosAnalisis() {
    try {
        const conn = await getConnection();
        const ResultadoConsulta1 = await conn.query('select SUM(Total) as total from ventas')

        const ResultadoConsulta2 = await conn.query('SELECT COUNT(*) as ganancias FROM ventas')

        const ResultadoConsulta3 = await conn.query('SELECT COUNT(*) as Clientes FROM clientes')

        const ResultadoConsulta4 = await conn.query('SELECT COUNT(*) as Envios FROM envios')

        window.webContents.on('did-finish-load', () => {
            window.webContents.send('TotalIngresos', ResultadoConsulta1)
            window.webContents.send('TotalVentasRealizadas', ResultadoConsulta2)
            window.webContents.send('TotalClientes', ResultadoConsulta3)
            window.webContents.send('TotalEnvios', ResultadoConsulta4)
        })

    } catch (error) {
        console.log("No sirvo Datos")
    }
}















//---------------------------------------CREAR VENTANA------------------------------------//
let window;
function CreateWindown() {
    window = new BrowserWindow({
        width: 800,
        height: 700,
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
            enableRemoteModule: true,
        },
    });
    window.loadURL(`file://${__dirname}/views/Login.html`);
    GetProducts();
    GetClientes();
    GetEnvios();
    GetProductsPuntoVenta();
    GetClientesPuntoVenta();
    GetFacturas();
    GetDetalle();
    GetDatosAnalisis();
    GetGrafico();
}


module.exports = {
    CreateWindown,
    GetProducts,
    CreateProduct,
    deleteProduct,
    getProductsById


}

